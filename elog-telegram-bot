#!/usr/bin/env python

import os
import telegram
from telegram.ext import Updater
from telegram.ext import CommandHandler, RegexHandler
import elog
import pickle
import sys
from io import BytesIO
import requests
import time
import html2text
import re
import base64
import html
import pytesseract
import PyPDF2
from PIL import Image
from wand.image import Image as wImage

sys.stdout = open(os.devnull, "w")

elog_token = os.environ["ELOGTOKEN"]
telegram_token = os.environ["TELEGRAMTOKEN"]

def save_obj(obj, filename):
    with open(filename, "wb") as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(filename):
    with open(filename, "rb") as f:
        return pickle.load(f)

def parse_log_entry(log_entry):
    log_entry['images']=[]
    log_entry['pdf']=[]
    message=log_entry['raw_text']
    images = re.findall(r"<img (.*?)/>", message)
    message = re.sub(r"<img (.*?)/>", r"", message)
    replace_html_list = re.findall(r"&(.*?);", message)
    for i in set(replace_html_list):
        html_to_replace = "&" + i + ";"
        message = re.sub(html_to_replace, html.unescape(html_to_replace), message)
    message = html2text.html2text(message)
    message = re.sub(r"\*\*(.*?)\*\*", r"<b>\1</b>", message)
    message = re.sub(r"\_(.*?)\_", r"<i>\1</i>", message)
    message = re.sub(r"<>", r"with", message)
    message = re.sub(r"https://elog.esss.lu.se/Operation/(\d+)", r"/get_\1", message)
    message = re.sub(r"<(.*?@.*?)>", r"\1", message)
    log_entry['message']=message
    log_entry['search']=message
    log_entry['search']+=' '+log_entry['metadata']['Author']
    log_entry['search']+=' '+log_entry['metadata']['Subject']
    if len(images) > 0:
        for i in images:
            if 'data:image/png;base64' in i:
                image = re.findall(r"data:image/png;base64,(.*?)\"", i)[0]
                log_entry['images'].append(BytesIO(base64.decodebytes(image.encode("utf-8"))))
            else:
                url = "https://elog.esss.lu.se/Operation/"+re.findall(r"src=\"(.*?)\"", i)[0]
                response = requests.get(url, headers={"Cookie":logbook._make_user_and_pswd_cookie()}, allow_redirects=False, verify=False)
                if response.status_code == 200:
                    log_entry['images'].append(BytesIO(response.content))
    for url in log_entry['attachments']:
        if "png" in url.lower() or "jpg" in url.lower() or "jpeg" in url.lower():
            response = requests.get(url, headers={"Cookie": logbook._make_user_and_pswd_cookie()}, allow_redirects=False, verify=False)
            if response.status_code == 200:
                log_entry['images'].append(BytesIO(response.content))
        if "pdf" in url.lower():
            response = requests.get(url, headers={"Cookie": logbook._make_user_and_pswd_cookie()}, allow_redirects=False, verify=False)
            if response.status_code == 200:
                log_entry['pdf'].append(BytesIO(response.content))
    for i in log_entry['images']:
        img = Image.open(i)
        log_entry['search']+=' '+pytesseract.image_to_string(img)
        img.close()
        
    for i in log_entry['pdf']:
        pdf_file = PyPDF2.PdfFileReader(i)
        for num_page in range(pdf_file.getNumPages()):
            tmp_pdf = PyPDF2.PdfFileWriter()
            tmp_pdf.addPage(pdf_file.getPage(num_page))
            page = BytesIO()
            tmp_pdf.write(page)
            page.seek(0)
            img = Image.open(BytesIO(wImage(file = page, resolution = 300).make_blob("png")))
            log_entry['search']+=' '+pytesseract.image_to_string(img)
            img.close()
        
    log_entry['search']=log_entry['search'].lower()
    log_entry['search']=re.sub(r'(\n\s*)', ' ', log_entry['search'])

def build_log_db(log_entries):
    for msg_id in range(logbook.get_last_message_id()+1):
        if msg_id in log_entries:
            if log_entries[msg_id]['raw_text']=='':
                try:
                    log_entries[msg_id]={}
                    log_entries[msg_id]['raw_text']=logbook.read(msg_id)[0]
                    log_entries[msg_id]['metadata']=logbook.read(msg_id)[1]
                    log_entries[msg_id]['attachments']=logbook.read(msg_id)[2]
                except:
                    log_entries[msg_id]['raw_text']=''
                    log_entries[msg_id]['metadata']={}
                    log_entries[msg_id]['attachments']=[]
                    log_entries[msg_id]['message']='No entry with ID '+str(msg_id)
                    log_entries[msg_id]['images']=[]
                    log_entries[msg_id]['pdf']=[]
                    log_entries[msg_id]['search']=''
                    continue
        else:
            try:
                log_entries[msg_id]={}
                log_entries[msg_id]['raw_text']=logbook.read(msg_id)[0]
                log_entries[msg_id]['metadata']=logbook.read(msg_id)[1]
                log_entries[msg_id]['attachments']=logbook.read(msg_id)[2]
            except:
                log_entries[msg_id]['raw_text']=''
                log_entries[msg_id]['metadata']={}
                log_entries[msg_id]['attachments']=[]
                log_entries[msg_id]['message']='No entry with ID '+str(msg_id)
                log_entries[msg_id]['images']=[]
                log_entries[msg_id]['pdf']=[]
                log_entries[msg_id]['search']=''
                continue
        print('Parsing Message',msg_id)
        parse_log_entry(log_entries[msg_id])

def chop_message(message):
    chunks = []
    while len(message) > 4096:
        end_point = message[:4096].rfind('\n')
        chunks.append(message[0:end_point])
        message = message[end_point+1:]
    chunks.append(message)
    return chunks

logbook = elog.open( "https://elog.esss.lu.se/Operation", user="srv_logbook", password="")
logbook._password = elog_token  # Trick to pass the digested password

log_entries_file = "log_entries"
if os.path.isfile(log_entries_file):
    log_entries = load_obj(log_entries_file)
else:
    log_entries = {}
    build_log_db(log_entries)
    save_obj(log_entries, log_entries_file)

senderbot = telegram.Bot(token=telegram_token)

registered_ids_file = "reg_ids"
if os.path.isfile(registered_ids_file):
    registered_ids = load_obj(registered_ids_file)
else:
    registered_ids = []

def send_message(chat_id, message):
    if len(message) == 0:
        return

    for chunks in chop_message(message):
        try:
            senderbot.sendMessage(chat_id=chat_id, text=chunks, parse_mode=telegram.ParseMode.HTML)
        except:
            return

updater = Updater(token=telegram_token, user_sig_handler=True)
dispatcher = updater.dispatcher

def start_function(bot, update):
    chat_id = update["message"]["chat"]["id"]
    first_name = update["message"]["chat"]["first_name"]
    family_name = update["message"]["chat"]["last_name"]

    if chat_id in registered_ids:
        message = "You are already connected to the bot.\n"
        message += "For the list of commands you can use /help."
    else:
        registered_ids.append(chat_id)
        save_obj(registered_ids, registered_ids_file)
        message = "Hi {} {}\n".format(first_name, family_name)
        message += "Welcome to the eLog Bot.\n"
        message += "You are now registered to receive the eLog updates from the ESS Operations eLog.\n"
        message += "But this Bot can help you also in browsing the eLog entries.\n\n"
        message += "For the list of available commands use /help"
    send_message(chat_id, message)

def help_function(bot, update):
    chat_id = update["message"]["chat"]["id"]
    print(chat_id)
    message = "/get_messageID will get the message with the messageID.\n"
    message += "Example /get_123.\n"
    message += "/last will tell you how many eLog entries are available.\n"
    message += "/search N keywords will give the list of the last N messages that contains the keywords. If N is 0 it gives all the results (it can be a long list).\n"
    message += "Example /search 5 EMU scan.\n"
    send_message(chat_id, message)

def last_function(bot, update):
    chat_id = update["message"]["chat"]["id"]
    message = "The latest entry in the logbook has ID: /get_" + str(max(log_entries.keys()))
    send_message(chat_id, message)

def get_function(bot, update):
    chat_id = update["message"]["chat"]["id"]
    msg_id = update["message"]["text"].replace("/get_", "")
    msg_id = int(msg_id)
    if msg_id > max(log_entries.keys()):
        send_message(chat_id, 'Last eLog entry is '+str(max(log_entries.keys())))
        return
    if len(log_entries[msg_id]['metadata']) == 0:
        send_message(chat_id, log_entries[msg_id]['message'])
        return
    message = "On " + log_entries[msg_id]['metadata']["Date"] + "\n"
    message += log_entries[msg_id]['metadata']["Author"] + "\n"
    if "In reply to" in log_entries[msg_id]['metadata'].keys():
        message += ( "In reply to message /get_" + log_entries[msg_id]['metadata']["In reply to"] + "\n")
    message += "Wrote the message:\n"
    message += "Subject: <b>" + log_entries[msg_id]['metadata']["Subject"] + "</b>\n"
    send_message(chat_id, message)
    send_message(chat_id, log_entries[msg_id]['message'])

    for img in log_entries[msg_id]['images']:
        img.seek(0)
        if img.getbuffer().nbytes < 10485760:
            try:
                senderbot.sendPhoto(chat_id=chat_id, photo=img)
            except:
                break
        else:
            try:
                senderbot.sendDocument(chat_id=chat_id, document=img)
            except:
                break
    pdf_counter = 0
    for pdf in log_entries[msg_id]['pdf']:
        pdf.seek(0)
        try:
            senderbot.sendDocument(chat_id=chat_id, document=pdf, filename='document-'+str(pdf_counter).zfill(3)+'.pdf')
        except:
            break
        pdf_counter+=1

def search_function(bot, update, args):
    chat_id = update["message"]["chat"]["id"]
    if not args[0].isdigit():
        send_message(chat_id, "The first argument must be an integer.\n")
        return

    send_message(chat_id, "Searching...\n")
    messages = []
    for msg_id in log_entries:
        try:
            text = log_entries[msg_id]['search']
        except:
            continue
        found = True
        for word in args[1:]:
            found = found and (word.lower() in text)
        if found:
            message = log_entries[msg_id]['metadata']["Date"] + "\n"
            message += "Subject: " + log_entries[msg_id]['metadata']["Subject"] + "\n"
            message += "Author: " + log_entries[msg_id]['metadata']["Author"] + "\n"
            message += "/get_" + str(msg_id) + "\n\n"
            if len(message) > 0:
                messages.append(message)

    messages = messages[-int(args[0]) :]

    start_msg = 0
    for end_msg in range(len(messages)):
        if len("".join(messages[start_msg:end_msg])) > 4096:
            send_message(chat_id, "".join(messages[start_msg : end_msg - 1]))
            time.sleep(0.1)
            start_msg = end_msg - 1
    if len(messages) > 0:
        send_message(chat_id, "".join(messages[start_msg : end_msg + 1]))
        send_message(chat_id, "Done.\n")
    else:
        send_message(chat_id, "No result found.\n")


dispatcher.add_handler(CommandHandler("start", start_function))
dispatcher.add_handler(CommandHandler("help", help_function))
dispatcher.add_handler(CommandHandler("last", last_function))
dispatcher.add_handler(RegexHandler("^(/get_.*)$", get_function))
dispatcher.add_handler(CommandHandler("search", search_function, pass_args=True))
updater.start_polling()

def update_logbook():
    latest_id_downloaded = max(log_entries.keys())
    latest_id_available = logbook.get_last_message_id()

    if latest_id_downloaded < latest_id_available:
        for msg_id in range(latest_id_downloaded + 1, latest_id_available + 1):
            try:
                log_entries[msg_id]={}
                log_entries[msg_id]['raw_text']=logbook.read(msg_id)[0]
                log_entries[msg_id]['metadata']=logbook.read(msg_id)[1]
                log_entries[msg_id]['attachments']=logbook.read(msg_id)[2]
            except:
                log_entries[msg_id]['raw_text']=''
                log_entries[msg_id]['metadata']={}
                log_entries[msg_id]['attachments']=[]
                log_entries[msg_id]['message']='No entry with ID '+str(msg_id)
                log_entries[msg_id]['images']=[]
                log_entries[msg_id]['pdf']=[]
                log_entries[msg_id]['search']=''

            parse_log_entry(log_entries[msg_id])
            save_obj(log_entries, log_entries_file)

            for chat_id in registered_ids:
                message = "**New entry with ID: " + str(msg_id) + "**\n"
                message += "On " + log_entries[msg_id]['metadata']["Date"] + "\n"
                message += log_entries[msg_id]['metadata']["Author"] + "\n"
                if "In reply to" in log_entries[msg_id]['metadata'].keys():
                    message += ( "In reply to message /get_" + log_entries[msg_id]['metadata']["In reply to"] + "\n")
                message += "Wrote the message:\n"
                message += ( "Subject: <b>" + log_entries[msg_id]['metadata']["Subject"] + "</b>\n")
                send_message(chat_id, message)
                send_message(chat_id, log_entries[msg_id]['message'])
                for img in log_entries[msg_id]['images']:
                    img.seek(0)
                    if img.getbuffer().nbytes < 10485760:
                        senderbot.sendPhoto(chat_id=chat_id, photo=img)
                    else:
                        senderbot.sendDocument(chat_id=chat_id, document=img)
                pdf_counter = 0
                for pdf in log_entries[msg_id]['pdf']:
                    pdf.seek(0)
                    senderbot.sendDocument(chat_id=chat_id, document=pdf, filename='document-'+str(pdf_counter).zfill(3)+'.pdf')
                    pdf_counter+=1

    if latest_id_downloaded > latest_id_available:
        for msg_id in range(latest_id_available + 1, latest_id_downloaded + 1):
            del log_entries[msg_id]
            save_obj(log_entries, log_entries_file)


while True:
    try:
        update_logbook()
    except:
        continue
    time.sleep(3)
